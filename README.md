# Custom Programmer's Dvorak

Duplicate accent symbols are normals and their dead counterparts.


#### Enable right alt key as well as turn capslock into ctrl/escape

`setxkbmap dvpc -option caps:ctrl_modifier -option lv3:ralt_switch`<br>
<br>
`xcape -e 'Caps_Lock=Escape'`<br>
Xcape can be found in some repos or it can be built from [SOURCE](https://github.com/alols/xcape).

<br>
Image generated with [ijprest's keyboard layout editor](https://github.com/ijprest/keyboard-layout-editor).
![Preview](preview.png)
